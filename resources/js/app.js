
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');
import Vue from 'vue'
import ElementUI from 'element-ui'
import Axios from 'axios'

import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
locale.use(lang)

//Vue.use(ElementUI)
Vue.use(ElementUI, {size: 'small'})
Vue.prototype.$eventHub = new Vue()
Vue.prototype.$http = Axios

Vue.component('companies-form', () => import('./views/companies/form.vue'));
Vue.component('users-index', () => import('./views/users/index.vue'));
Vue.component('options-form', () => import('./views/options/form.vue'));
Vue.component('certificates-index', () => import('./views/certificates/index.vue'));
Vue.component('certificates-form', () => import('./views/certificates/form.vue'));
Vue.component('documents-index', () => import('./views/documents/index.vue'));
Vue.component('retentions-index', () => import('./views/retentions/index.vue'));
Vue.component('perceptions-index', () => import('./views/perceptions/index.vue'));
Vue.component('dispatches-index', () => import('./views/dispatches/index.vue'));
Vue.component('search-index', () => import('./views/search/index.vue'));
Vue.component('summaries-index', () => import('./views/summaries/index.vue'));

const app = new Vue({
    el: '#main-wrapper'
});


